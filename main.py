from brain.listener import Listener
from brain.knowledges.salutation import Salutation

if __name__ == '__main__':
    Salutation()
    listen = Listener()
    while True:
        query = listen.getCommand()
        listen.doAction(query)