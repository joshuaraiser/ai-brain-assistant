import pyttsx3

class Speaker:

    def __init__(self, sentence = None):
        self.api = 'sapi5'
        self.rate = 210
        self.engine = pyttsx3.init(self.api)
        self.voice = self.engine.getProperty('voices')[0].id

        self.engine.setProperty('voice', self.voice)
        self.engine.setProperty('rate', self.rate)

        if sentence is not None:
            self.speak(sentence)

    def speak(self, sentence):
        self.engine.say(sentence)
        self.engine.runAndWait()