from brain.speaker import Speaker as speak
from datetime import datetime as time

class Information():

    def __init__(self, needed):

        if 'horas' in needed:
            speak('Agora são ' + time.now().strftime('%H:%M:%S'))
        
        else:
            speak('Não possuo essa informação!')