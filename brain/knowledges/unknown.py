from brain.speaker import Speaker as speak

class Unknown():

    def __init__(self, message = None):
        if message is not None:
            speak(message)
        else:
            speak('Não entendi! Pode repetir, por favor?')