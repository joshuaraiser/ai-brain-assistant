from brain.speaker import Speaker as speak

import wikipedia

class Search():

    def __init__(self, query):
        self.query = query

    def getResult(self):
        wikipedia.set_lang("pt")

        try:
            speak(wikipedia.summary(self.query, sentences=4))

        except:
            speak('Não encontrei nada a respeito!')