from brain.speaker import Speaker as speak
from datetime import datetime as time

class Salutation():

    def __init__(self):
        now = time.now()
        
        if int(now.hour) >= 0 and int(now.hour) < 12:
            speak('Bom dia!')

        elif int(now.hour) >= 12 and int(now.hour) < 18:
            speak('Boa tarde!')

        else:
            speak('Boa noite!')