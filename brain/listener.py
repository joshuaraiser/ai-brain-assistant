from brain.knowledges.unknown import Unknown
from brain.knowledges.search import Search
from brain.knowledges.information import Information

import speech_recognition as sr

class Listener():

    def __init__(self):
        self.rec = sr.Recognizer()
        self.mic = sr.Microphone()

    def getCommand(self):
        with self.mic as source:
            self.rec.pause_threshold = 1
            audio = self.rec.listen(source)

        try:
            query = self.rec.recognize_google(audio, language='pt-BR')

        except:
            return None

        return query.lower()

    def doAction(self, command):
        if command is None:
            Unknown()

        elif command is not None:

            if 'pesquisar' in command:
                search = Search(command.replace('pesquisar', ''))
                search.getResult()
            
            if 'pode me informar' in command:
                Information(command)
            
            
            


        


        