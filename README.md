# ai-brain-assistant

AI Brain Assistant is a virtual assistant that can do many things:

  - Provide the hours
  - Search by specific terms on Wikipedia
  - TODO: add more things ...

### Tech

AI Brain Assistant uses a number of projects to work properly:

* [python 3.6]            - Python program language version 3.6
* [pyttsx3]           - An OFFLINE Python Text to Speech
* [speechRecognition] - Library for performing speech recognition, with support for several engines and APIs, online and offline
* [wikipedia]         - Wikipedia is a Python library that makes it easy to access and parse data from Wikipedia

### Installation

AI Brain Assistant requires [python 3.6] to run perfectly.

Install the dependencies:

```sh
$ python pip install pyttsx3
$ python pip install SpeechRecognition
$ python pip install wikipedia
```
And run it:

```sh
$ python main.py
```

######
---

### Future

TODO: 

To the future of this project:

- api to integrate a mobile app
    -  By the api the AI Brain Assistant will receive information and send too
- mobile app
    - By the mobile app AI Brain Assistant will collect some information and notify users
 

[pyttsx3]: <https://pypi.org/project/pyttsx3/2.7/>
[python 3.6]: <https://www.python.org/downloads/release/python-360/>
[speechRecognition]: <https://pypi.org/project/SpeechRecognition/>
[wikipedia]: <https://pypi.org/project/wikipedia/>